from django.db import models

# Create your models here.
class Video(models.Model):
	canal = models.CharField(max_length=120)
	titulo = models.CharField(max_length=120)
	id = models.CharField(max_length=120, primary_key = True)
	link = models.CharField(max_length=120)
	imagen = models.CharField(max_length=120)
	fecha = models.CharField(max_length=120)
	descripcion = models.CharField(max_length=120)
	selected= models.BooleanField(default = False)


